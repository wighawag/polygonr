package polygonr.model;

import flash.display.BitmapData;
import flash.geom.Point;
import flash.geom.Rectangle;
import haxe.Json;

/**
 * SpriteSheet parsers / helpers
 * @author Mike Almond - https://github.com/mikedotalmond
 */

class SpriteSheet {  }


/**
 * Starling/Sparrow TextureAtlas parser
 * Outputs a Map<String,BitmapData>
 *
 * Does not support frame regions or sprite rotation
 */
@:final class StarlingSpriteSheet {

	public static function parse(atlasData:String, bitmap:BitmapData):Map<String, BitmapData> {

		bitmap.lock();

		var atlasXml 	= Xml.parse(atlasData);
		var map 		= new Map<String, BitmapData>();

		for (el in atlasXml.elements()) {
			if (el.nodeName == 'TextureAtlas') {
				var textures = el.elementsNamed('SubTexture');
				for (sub in textures) {
					var name = sub.get('name');
					map.set(
						name,
						new SubTexture(bitmap,
							Std.parseInt(sub.get('x')),
							Std.parseInt(sub.get('y')),
							Std.parseInt(sub.get('width')),
							Std.parseInt(sub.get('height'))
						)
					);
				}
			}
		}

		return map;
	}
}


@:final class SubTexture extends BitmapData {

	// location within the parent
	public var x(default, null):Int;
	public var y(default, null):Int;
	
	public function new(source:BitmapData, x:Int, y:Int, w:Int, h:Int) {
		super(w, h, true, 0);
		this.x = x; this.y = y;
		copyPixels(source, new Rectangle(x, y, w, h), new Point());
	}
}


/**
 * Compatible with the JSON data format used in PixieJS, UIToolkit, NGUI (Unity)
 */
@:final class JSSpriteSheet {

	public static function parse(data:String, bitmap:BitmapData):Map<String, BitmapData> {
		
		var obj = Json.parse(data);
		var map = new Map<String, BitmapData>();

		if (isFlump(obj)) {
			parseFlump(map, obj, bitmap);
		} else if (isPixie(obj)) {
			parsePixie(map, obj, bitmap);
		} else {
			throw 'Unrecognised JSON data';
		}
		
		return map;
	}
	
	static public function getLibraryAssetsFileNames(data:String) {
		
		var obj = Json.parse(data);
		var out:Array<String> = [];
		
		if (isFlump(obj)) {
			var textureGroups:Array<Dynamic> = Reflect.getProperty(obj, 'textureGroups');		
			for (group in textureGroups) {
				var atlases:Array<Dynamic> = Reflect.getProperty(group, 'atlases');
				for (atlas in atlases) {
					out.push(Reflect.getProperty(atlas, 'file')); // filename for current bitmap
				}
			}
		}
		
		return out;
	}
	
	static private function parseFlump(map:Map<String, BitmapData>, obj:Dynamic, bitmap:BitmapData) {
		
		var textureGroups:Array<Dynamic> = Reflect.getProperty(obj, 'textureGroups');		
		if (textureGroups.length > 1) trace('More than one item in textureGroups'); // scaled alternatives....
		
		var group = textureGroups[0];
		// only expect one texture group....
		//for (group in textureGroups) {
			var atlases:Array<Dynamic> = Reflect.getProperty(group, 'atlases');
			for (atlas in atlases) {
				//Reflect.getProperty(atlas, 'file'); // filename for current bitmap
				var textures:Array<Dynamic> = Reflect.getProperty(atlas, 'textures');
				for (texture in textures) {
					var rect:Array<Int> 	= Reflect.getProperty(texture, 'rect');
					var name:String 		= Reflect.getProperty(texture, 'symbol');			
					map.set(name, new SubTexture(bitmap, rect[0], rect[1], rect[2], rect[3]));
				}
			}
		//}
	}
	
	
	static private function parsePixie(map:Map<String, BitmapData>, obj:Dynamic, bitmap:BitmapData) {
		
		var frames 	= Reflect.getProperty(obj, 'frames');
		var names 	= Reflect.fields(frames);
		
		for (name in names) {
			var frame = Reflect.field(Reflect.field(frames, name), 'frame');
			map.set(
				name,
				new SubTexture(bitmap,
					Reflect.getProperty(frame, 'x'),
					Reflect.getProperty(frame, 'y'),
					Reflect.getProperty(frame, 'w'),
					Reflect.getProperty(frame, 'h')
				)
			);
		}
	}
	
	static function isFlump(data:Dynamic):Bool {
		return (
			Reflect.hasField(data, 'textureGroups') &&
			Reflect.hasField(data, 'movies') &&
			Reflect.hasField(data, 'md5') &&
			Reflect.hasField(data, 'frameRate')
		);
	}
	
	static function isPixie(data:Dynamic):Bool {
		return (
			Reflect.hasField(data, 'frames') &&
			Reflect.hasField(data, 'meta')
		);	
	}
}