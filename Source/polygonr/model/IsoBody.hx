package polygonr.model;

import flash.display.BitmapData;
import haxe.io.Bytes;
import nape.geom.AABB;
import nape.geom.GeomPoly;
import nape.geom.GeomPolyList;
import nape.geom.IsoFunction.IsoFunctionDef;
import nape.geom.MarchingSquares;
import nape.geom.Vec2;
import nape.phys.Body;
import nape.shape.Polygon;

typedef Decomposer = GeomPoly->GeomPolyList->Void;

enum DecompositionMode {
	Simple;
	Monotone;
	Convex;
	ConvexDelaunay;
	Triangular;
	TriangularDelaunay;
}

/**
 * ...
 * @author Mike Almond - https://github.com/mikedotalmond
 */
@:final class IsoBody {
	
	
	/**
	 * @param	iso
	 * @param	bounds
	 * @param	granularity
	 * @param	quality
	 * @param	simplification
	 * @param	decompositionMode
	 *
	 * For more info, see: http://napephys.com/help/manual.html ~~ 9. Geometric Utilities
	 */
    public static function run(iso:IsoFunctionDef, bounds:AABB, granularity:Vec2=null, quality:Int=2, simplification:Float=2, decompositionMode:DecompositionMode = null) {
		
		var body = new Body();
        if (granularity == null) granularity = Vec2.get(8, 8);
		
		var decompose = getDecomposer(decompositionMode == null ? DecompositionMode.Convex : decompositionMode);
		
		var minPolyArea = 1.0; // polys with an area less than 1x1 pixels are discarded. 
        var polys:GeomPolyList = MarchingSquares.run(iso, bounds, granularity, quality);
		
		var qolys = new GeomPolyList();
		
        for (p in polys) {
			
			try {
				
				p = p.simplify(simplification);
				
				decompose(p, qolys);
				
				for (q in qolys) {
					if (q.area() >= minPolyArea) body.shapes.add(new Polygon(q));
					// Recycle GeomPoly and its vertices
					q.dispose();
				}
				
			} catch (err:Dynamic) {
				trace('Unable to decompose poly.');
				//trace('size:${p.size()}, empty:${p.empty()}, area:${p.area()}, isConvex:${p.isConvex()}, isMonotone:${p.isMonotone()}, isSimple:${p.isSimple()}, isDegenerate:${p.isDegenerate()}');
			}
			
			// Recycle list nodes
			qolys.clear();
			// Recycle GeomPoly and its vertices
			p.dispose();
        }

        // Recycle list nodes
        polys.clear();
		
        // Align body with its centre of mass.
        // Keeping track of our required graphic offset.
        var pivot = body.localCOM.mul(-1);
        body.translateShapes(pivot);
        body.userData.graphicOffset = pivot;
		
        return body;
    }
	
	
	// There are various options for deomposition mode...
	static function getDecomposer(mode:DecompositionMode):Decomposer {
		return switch(mode) {
			
			case DecompositionMode.Simple: 
				function(poly:GeomPoly, out:GeomPolyList) { poly.simpleDecomposition(out); }
				
			case DecompositionMode.Monotone:
				function(poly:GeomPoly, out:GeomPolyList) { poly.monotoneDecomposition(out); }
				
			case DecompositionMode.Convex:
				function(poly:GeomPoly, out:GeomPolyList) { poly.convexDecomposition(false, out); }
				
			case DecompositionMode.ConvexDelaunay:
				function(poly:GeomPoly, out:GeomPolyList) { poly.convexDecomposition(true, out); }
				
			case DecompositionMode.Triangular:
				function(poly:GeomPoly, out:GeomPolyList) { poly.triangularDecomposition(false, out); }
				
			case DecompositionMode.TriangularDelaunay:
				function(poly:GeomPoly, out:GeomPolyList) { poly.triangularDecomposition(true, out); }
				
		}
	}
}
