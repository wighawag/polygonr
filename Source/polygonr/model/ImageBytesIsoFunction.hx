package polygonr.model;
import haxe.io.Bytes;
import nape.geom.AABB;

/**
 * ...
 * @author Mike Almond - https://github.com/mikedotalmond
 */

@:final class ImageBytesIsoFunction {
	
	public var bytes:Bytes;
    public var width:Int;
    public var height:Int;
    public var threshold:Int;
    public var bounds:AABB;
	
	/**
	 *
	 * @param	bytes - expects BGRA
	 * @param	width
	 * @param	height
	 * @param	threshold
	 */
    public function new(bytes:Bytes, width:Int, height:Int, threshold:Int = 0x80) {
        this.bytes 		= bytes;
        this.width 		= width;
        this.height 	= height;
        this.threshold 	= threshold;
        bounds 			= new AABB(0, 0, width, height);
    }
	
	// http://napephys.com/docs/types/nape/geom/MarchingSquares.html
    public function iso(x:Float, y:Float) {
		
		var ix = if (x < 0) 0 else if (x >= width - 1) width - 2 else Std.int(x);
		var iy = if (y < 0) 0 else if (y >= height - 1) height - 2 else Std.int(y);
		
		// iso-function values at each pixel centre.
		var a00 = bytes.get(((ix + width * iy) << 2) + 3);
		var a01 = bytes.get(((ix + width * (iy + 1)) << 2) + 3);
		var a10 = bytes.get((((ix + 1) + width) << 2) + 3);
		var a11 = bytes.get((((ix + 1) + width * (iy + 1)) << 2) + 3);
		
		// Bilinear interpolation for sample point (x,y)
        var fx = x - ix; var fy = y - iy;
		var gx = 1 - fx; var gy = 1 - fy;
		
		return threshold - (gx * gy * a00 + fx * gy * a10 + gx * fy * a01 + fx * fy * a11);
    }
}