package polygonr.model;

import flash.display.BitmapData;
import nape.geom.AABB;

/**
 * ...
 * @author Mike Almond - https://github.com/mikedotalmond
 */

@:final class BitmapDataIsoFunction {
	
	public var bitmap:BitmapData;
    public var width:Int;
    public var height:Int;
    public var threshold:Int;
    public var bounds:AABB;
	
    public function new(bitmap:BitmapData, threshold:Int = 0x80, ?bounds:AABB=null) {
        this.bitmap 	= bitmap;
        this.width 		= bitmap.width;
        this.height 	= bitmap.height;
        this.threshold 	= threshold;
        this.bounds 	= bounds == null ? new AABB(-.5, -.5, width + 1, height + 1) : bounds;
    }
	
	
	// http://napephys.com/docs/types/nape/geom/MarchingSquares.html
	public function iso(x:Float, y:Float) {
		
		var ix = if (x < 0) 0 else if (x >= bitmap.width - 1) bitmap.width - 2 else Std.int(x);
		var iy = if (y < 0) 0 else if (y >= bitmap.height - 1) bitmap.height - 2 else Std.int(y);
		
		var a00 = bitmap.getPixel32(ix, iy) >>> 24;
		var a01 = bitmap.getPixel32(ix, iy + 1) >>> 24;
		var a10 = bitmap.getPixel32(ix + 1, iy) >>> 24;
		var a11 = bitmap.getPixel32(ix + 1, iy + 1) >>> 24;
		
		var fx = x - ix; var fy = y - iy;
		var gx = 1 - fx; var gy = 1 - fy;

		return threshold - (gx * gy * a00 + fx * gy * a10 + gx * fy * a01 + fx * fy * a11);
	}
}