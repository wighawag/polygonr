Polygonr
===

A little tool to create polygons (point-lists) from transparent PNG sprites and spritesheets.

Export as JSON (or copy to clipboard) for use in your game / whatever.

Built using Haxe, OpenFL + HaxeUI, and Nape for the geometry tools.

* [Haxe](http://haxe.org/)
* [OpenFl](http://www.openfl.org/)
* [haxeui](http://haxeui.org/)
* [Nape](http://napephys.com/)

Needs [my fork of HaxeUI](https://github.com/mikedotalmond/haxeui/) where I've modified the FilesDataSource a bit.

Download
===
A windows build is available on the [downloads](https://bitbucket.org/mikedotalmond/polygonr/downloads) page.

Screens
===
![Select an image](https://bitbucket.org/repo/8garAr/images/965357583-polygonr-select.PNG "Select")

![Process an image](https://bitbucket.org/repo/8garAr/images/1950016247-polygonr-process.PNG "Process")

![About Polygonr](https://bitbucket.org/repo/8garAr/images/1865640061-polygonr-about.PNG "About")